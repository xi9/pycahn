# Contributing to `PyCahn`

Any work is appreciated, but as this project combines complex code with complex math and physics, there are a few guidelines you must adhere to in order to make your work accessible to others.

## Follow PEP8

Adherence to PEP8 is *required* and PRs are checked by [`flake8`](https://flake8.pycqa.org/en/latest/) for conformance.
Non-adhering PRs will not be accepted.

## Use Type Annotations

Type annotations are *required* and PRs are checked by [`mypy`](http://mypy-lang.org/) for conformance.
Non-adhering PRs will not be accepted.

## Naming schemes

In general, name things what they are.
Prefer longer, descriptive names that enlighten readers of the problem context such as `population_standard_deviation` over `sigma` or `s`.
It is my opinion that descriptive inline comments are mainly a code smell from bad naming.

You may use `x` in list comprehensions or `i` for counters.

Attach a unit to a name when applicable.

### Good Name Examples
```python
diffusivity_meters_squared_per_second: float = 1.0
time_seconds: float = 10.0
acceleration_meters_per_second_squared: float = 9.8
mean_wavenumber_per_meter: float = 8.16
```

