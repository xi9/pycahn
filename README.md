# `PyCahn`

`PyCahn` is a system written in Python for the simulation of spinodal decomposition / phase separation using the Cahn-Hilliard equation.

## how can I use it?

There are no published packages of this software anywhere so you will have to clone the repository in order to use it:

```bash
git clone https://gitlab.com/xi9/cahn-hilliard.git
```

## how can I help?

Any contributions are appreciated greatly.
see [CONTRIBUTING.md](./CONTRIBUTING.md) for contribution guidelines.
