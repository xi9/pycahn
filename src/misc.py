'''
Miscellaneous functions and utilities used in the codebase
'''

import numpy as np
from typing import *


def newton_heat_closure(
        initial_temperature: float,
        final_temperature: float,
        decay_factor: float) -> Callable[[float], float]:
    '''
    A closure that generates a newton heating/cooling function given an initial
    temperature, final temperature, and decay factor.
    '''
    newton: Callable[[float], float] = lambda time: (
        final_temperature + (initial_temperature - final_temperature)
        * np.exp(-time / decay_factor)
    )
    return newton


def spinner(
        prelude: str,
        callback: Callable[[Any], Any],
        callback_args: Union[Dict[[str], Any], List[Any]] = {},
        input_range: range = range(1),
        argstyle: str = 'dict'):
    spinner_states = ['⠁', '⠃', '⠂', '⠆', '⠄',
                      '⠤', '⠠', '⠰', '⠐', '⠘', '⠈', '⠉']
    for i in input_range:
        if i == input_range[-1]:
            print(prelude+' ✓       ')
        else:
            percent = (i/input_range[-1]) * 100
            current_spinner = spinner_states[i % len(spinner_states)]
            print(prelude+' '+current_spinner +
                  ' ({0:.0f}%)'.format(percent), end='\r')
        if argstyle == 'dict':
            callback(**callback_args)
        if argstyle == 'list' or argstyle == 'tuple':
            callback(*callback_args)


def save_graph_output():
