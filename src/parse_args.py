'''
Anything and everything to do with parsing commandline arguments.
'''
from typing import *
import datetime
from pathlib import Path
from dataclasses import dataclass
import argparse
import numpy as np
from domain import SpectralDomain
from system import CahnHilliardSystem
from simulation import CahnHilliardSpectralSimulation


def process_vargs() -> Optional[Dict[str, Any]]:
    return process_args(**vars(get_vargs()))


def get_vargs():
    '''
    Configures argparse and parses command-line arguments into parameters
    '''
    parser = argparse.ArgumentParser(
        prog='pycahn',
        description='A spectral method Cahn-Hilliard simulation engine',
    )
    parser.add_argument(
        '-j', '--json',
        help='An input json settings file. Any settings additionally'
             ' specified on the command-line will override those in the file',
        metavar='FILE',
        nargs=1
    )
    parser.add_argument(
        '-r', '--random-seed',
        help='A seed to use for randomly generating initial conditions in a'
             ' repeatable way. The system time is used as the default.',
        metavar='SEED',
        nargs=1
    )
    parser.add_argument(
        '-w', '--widths',
        help='The widths of each spatial dimension, in x y z order',
        nargs='+',
        metavar='WIDTH',
        action='extend'
    )
    parser.add_argument(
        '-d', '--divisions',
        help='The number of divisions along each spatial dimension, in x y z'
             ' order.\n'
             'If an input file is not used, this must be specified and match'
             ' the number of spatial widths specified by the -w flag.',
        nargs='+',
        metavar='DIV',
        action='extend'
    )
    parser.add_argument(
        '-f', '--free-energy-function',
        help='A function in terms of the variable c that defines the free'
             ' energy of the system as a string',
        metavar='FXN',
        nargs=1
    )
    parser.add_argument(
        '-i', '--iterations',
        help='The maximum number of spectral simulation steps to take',
        metavar='ITER',
        nargs=1
    )
    parser.add_argument(
        '-t', '--time-step',
        help='The time difference for a step. Dimensionless',
        metavar='TIME',
        nargs=1
    )
    parser.add_argument(
        '-g', '--gamma',
        help='The interfacial energy term in the Cahn-Hilliard equation.',
        nargs=1
    )
    parser.add_argument(
        '-T', '--use-temperature-function',
        help='gamma is calculated automatically using a block copolymer model'
             ' and newton heating/cooling',
        action='store_true'
    )
    parser.add_argument(
        '-k', '--temperature-range',
        help='The initial and final temperature to use with the newton heating/cooling function',
        metavar='K',
        nargs=2
    )
    parser.add_argument(
        '-a', '--a-values',
        help='block copolymer model parameters, only used if the temperature'
             ' function is active',
        metavar='A',
        nargs=3
    )
    parser.add_argument(
        '-c', '--no-csv-output',
        help='a switch for specifying that CSVs of the output should NOT be created',
        action='store_true'
    )
    parser.add_argument(
        '-s', '--svg-output',
        help='a switch for generating svgs of composition values as a surface',
        action='store_true'
    )
    parser.add_argument(
        '-e', '--save-every',
        help='Generates output only on every Eth simulation step.',
        nargs=1
    )
    parser.add_argument(
        'DIR',
        help='the optional name of the output directory to place files in'
             ' (if it does not exist, it will be created)\n'
             'A name will be generated automatically if one is not specified',
        nargs='?'
    )
    return parser.parse_args()


def process_args(
    random_seed: Optional[List[str]] = None,
    widths: Optional[List[str]] = None,
    divisions: Optional[List[str]] = None,
    free_energy_function: Optional[List[str]] = None,
    iterations: Optional[List[str]] = None,
    time_step: Optional[List[str]] = None,
    gamma: Optional[List[str]] = None,
    use_temperature_function: bool = False,
    temperature_range: Optional[List[str]] = None,
    a_values: Optional[List[str]] = None,
    no_csv_output: bool = False,
    svg_output: bool = False,
    save_every: List[str] = None,
    DIR: Optional[str] = None,
) -> Optional[Dict[str, Any]]:
    '''
    Converts the raw output of `argparse.parse_args()` from
    `get_vargs()` to the actual types that they need to be in, and checks that
    '''
    try:
        if (not (divisions is None or widths is None)
                and len(divisions) != len(widths)):
            raise ValueError(
                'dimensions of the specified widths and divisions'
                ' do not match')
        if gamma is None and not use_temperature_function:
            raise ValueError(
                'Either a gamma must be specified or the temperature function'
                ' must be activated.'
            )
        if use_temperature_function and (a_values is None or temperature_range is None):
            raise ValueError(
                'a values and temperature range must be specified'
                ' if temperature funcion is active.'
            )

        packed: Dict[str, Any] = {}
        # pack the domain and system to hand off.
        SpectralDomain(
            [float(w) for w in widths] if widths is not None else [2.0, 2.0],
            [int(n) for n in divisions] if divisions is not None else [600, 600]
        )
        CahnHilliardSystem(
            [float(a) for a in a_values] if a_values is not None else [],
            make_free_energy_function(
                free_energy_function[0] if free_energy_function is not None else 'c**3 - 3*c'
            ),
            make_temperature_function(
                temperature_function[0]) if temperature_function is not None else None,
        )

        time_stamp = int(datetime.datetime.now().timestamp())
        packed['random_seed'] = int(
            random_seed[0]) if random_seed is not None else time_stamp
        packed['free_energy_str'] = free_energy_function[0] if free_energy_function is not None else 'c**3 - 3*c'
        packed['free_energy_function'] = make_free_energy_function(
            packed['free_energy_str'])
        packed['iterations'] = int(
            iterations[0]) if iterations is not None else 1000
        packed['time_step'] = float(
            time_step[0]) if time_step is not None else 0.001
        if gamma is not None:
            packed['gamma'] = float(gamma[0])
        packed['use_temperature_function'] = use_temperature_function
        if temperature_range is not None:
            packed['temperature_range'] = [float(x) for x in temperature_range]
        if a_values is not None:
            packed['a_values'] = [float(x) for x in a_values]
        packed['no_csv_output'] = no_csv_output
        packed['svg_output'] = svg_output
        packed['save_every'] = int(
            save_every[0]) if save_every is not None else 10

        if DIR is not None:
            specified_dir = Path(DIR[0])
            if specified_dir.resolve().is_absolute():
                packed['DIR'] = specified_dir
            else:
                packed['DIR'] = Path.cwd() / specified_dir
        else:
            packed['DIR'] = Path.cwd() / str(time_stamp)
        # we have to test the free energy function to make sure it's an actual function.
        testval = np.linspace(0, 1, 1000)
        packed['free_energy_function'](testval)
    except ValueError as e:
        print('Bad Arguments!')
        print(e)
        return None
    except SyntaxError as e:
        print('Bad free energy function!')
        print(e)
        print(
            'Did you format your function in terms of c?\n'
            'Did you use python notation?\n'
            'e.g. \'c**3 - 3*c\'')
        return None
    except NameError as e:
        print('Bad free energy function!')
        print(e)
        print(
            'Did you format your function in terms of c?\n'
            'Did you use python notation?\n'
            'e.g. \'c**3 - 3*c\'')
        return None
    else:
        print(packed)
        return packed


def make_free_energy_function(_input: str) -> Callable[[np.ndarray], np.ndarray]:
    return lambda c: eval(_input)
