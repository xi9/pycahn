'''
An object encapsulating the domain of a spectral simulation
'''
from typing import List
import numpy as np  # type: ignore


class SpectralDomain:
    '''
    An object encapsulating the domain of a spectral simulation.
    Includes calculation of the eigenvalue of the laplacian operator
    in spectral space (via the DCT).
    '''
    space_widths: List[float]
    '''
    The widths of each spatial dimension, in x y z order.
    '''
    space_divisions: List[int]
    '''
    The divisions along each spatial dimension, in x y z order.
    '''
    dimension: int
    '''
    The dimensionality of the entire domain.
    '''
    spatial: List[np.ndarray]
    '''
    The actual discretized spatial domain.
    '''
    spectral: List[np.ndarray]
    '''
    The actual discretized spectral domain.
    '''
    spatial_steps: List[float]
    '''
    The distance between discretized points along a spatial dimension,
    in x y z order.
    '''
    spectral_steps: List[float]
    '''
    The distance between discretized frequencies along a spectral domain,
    in x y z order.
    '''
    mean_spatial_step: float
    '''
    The average distance between discretized points across all dimensions.
    '''
    mean_spectral_step: float
    '''
    The average difference between discretized frequencies across all
    dimensions.
    '''
    laplacian_eigenvalue: np.ndarray
    '''
    The eigenvalue of the laplacian operator in the spectral domain.
    '''

    def __init__(self, space_widths: List[float], space_divisions: List[int]):
        '''
        instantiate and generate.
        '''
        input_dimension = len(space_widths)

        self.dimension = input_dimension
        self.space_widths = space_widths
        self.space_divisions = space_divisions
        dimensions_combo = zip(self.space_widths, self.space_divisions)
        self.spatial = [np.linspace(-0.5 * width, 0.5 * width, division)
                        for width, division in dimensions_combo]
        self.spectral = [np.linspace(0, (division - 1) / width, division)
                         for width, division in dimensions_combo]
        self.spatial_steps = [width/(division - 1)
                              for width, division in dimensions_combo]
        self.spectral_steps = [width/(division - 1)**2
                               for width, division in dimensions_combo]
        self.mean_spatial_step = sum(self.spatial_steps) / self.dimension
        self.mean_spectral_step = sum(self.spectral_steps) / self.dimension
        # I think this is the generalization out to n-dimensions, but I need
        # to actually work through the math. In any case, it definitely works
        # for 2D.
        # TODO: derive that this is correct for the N-dimensional case.
        self.laplacian_eigenvalue = (-sum(self.spectral)
                                     * np.pi**(self.dimension))
