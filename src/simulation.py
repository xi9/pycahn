'''
Functions and classes used in a spectral method Cahn-Hilliard simulation
'''

from typing import Callable
import numpy as np  # type: ignore
from scipy.fft import dctn, idctn  # type: ignore
from domain import SpectralDomain
from system import CahnHilliardSystem


class CahnHilliardSpectralSimulation:
    '''
    A functor class that runs a Cahn-Hilliard simulation given all the
    appropriate parameters.
    '''
    domain: SpectralDomain
    '''
    Class that stores domain information
    '''
    system: CahnHilliardSystem
    '''
    Class that stores system information
    '''
    composition: np.ndarray
    '''
    The composition of the system in the spatial domain
    '''
    time_step: float
    '''
    The time step with each iteration of the simulation.
    '''
    maximum_iterations: int
    '''
    The maximum number of iterations to do
    '''
    time: float = 0
    '''
    The current simulation time
    '''
    gamma_function: Callable[[float], float]
    '''
    The function in use to calculate gamma.
    May either be time-dependent or independent, that's all figured out by the
    CahnHilliardSystem class.
    '''

    def __init__(self, domain, system, initial_composition,
                 time_step, maximum_iterations):
        self.domain = domain
        self.system = system
        self.composition = initial_composition
        self.time_step = time_step
        self.gamma_function = system.get_gamma_function(domain.mean_space_step)

    def step_simulation(self, callback: Callable[[np.ndarray], None]):
        '''
        Steps the spectral method simulation by one time-step.
        Callback is called on the result at the end of the step.
        '''
        self.time += self.time_step
        next_dct_composition = self.next_dct_composition()
        self.composition = idctn(next_dct_composition, norm='ortho')
        callback(self.composition)

    def next_dct_composition(self):
        ''' 
        '''
        prev_dct_composition = dctn(self.composition, norm='ortho')
        composition_energy = self.system.free_energy_function(self.composition)
        denominator = self.cheig()
        numerator = (
            prev_dct_composition
            + self.time_step * self.domain.laplacian_eigenvalue *
            dctn(composition_energy, norm='ortho')
        )
        return numerator / denominator

    # TODO: Needs a better name. Cheig is not really an eigenvalue.
    # TODO: Needs generalized to N-dimensions. How?
    def cheig(self):
        return (
            np.ones(self.domain.spatial_domains[0],
                    self.domain.spatial_domains[1])
            - 2 * self.time_step * self.gamma_function(self.time) *
            self.domain.laplacian_eigenvalue**2
        )

    def run_simulation(self, callback: Callable[[np.ndarray], None]):
        '''
        Runs a whole cahn-hilliard simulation, calling `callback` with the
        output composition array on every iteration
        '''
        for _ in range(self.maximum_iterations):
            self.step_simulation(callback)
