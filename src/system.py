'''
An encapsulating class capturing the physical system to simulate
'''

from typing import List, Callable, Optional
from dataclasses import dataclass
import numpy as np


@dataclass
class CahnHilliardSystem:
    '''
    An encapsulating class capturing the physical system to simulate
    '''
    a_values: List[float]
    '''
    Polymer blend model parameters that change gamma with the temperature.
    '''
    free_energy_function: Callable[[np.ndarray], np.ndarray]
    '''
    A function describing the Gibbs free energy with respect to composition
    '''
    temperature_function: Optional[Callable[[float], float]] = None
    '''
    A function describing the temperature of a system with respect to time
    '''
    gamma: Optional[float] = None
    '''
    The interfacial energy. Use the get_gamma method instead of accessing this directly
    '''

    def __post_init__(self):
        '''
        Checks sanity of input parameters
        '''
        if not self.gamma and not self.temperature_function:
            raise ValueError('If gamma is not specified, a temperature'
                             'function must be given')

    def get_gamma_function(self, mean_space_step: float) -> Callable[[float], float]:
        '''
        Returns a function with respect to time that defines gamma.
        '''
        if self.temperature_function is not None:
            chi: Callable[[float], float] = (lambda time: self.a_values[1] +
                                             self.a_values[2] /
                                             self.temperature_function(time))
            return lambda time: (self.a_values[0]**2
                                 * chi(time) * mean_space_step)
        else:
            return lambda time: self.gamma
